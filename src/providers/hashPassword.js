import md5 from 'md5'
import {sha256} from 'js-sha256'



class hashPassword {
    static hashingPassword(password){
        const salt =  '2sgf6j8kp1l';
        let saltPassword =  salt + password;
        let passwordMd5 =  salt + md5(saltPassword);
        let passwordSha256 =  sha256(passwordMd5);
        return  passwordSha256;
    }
}

export default hashPassword;