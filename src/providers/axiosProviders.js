import axios from 'axios'

class axiosProviders{

    static async createRequest(url,type,data){
        console.log(data);
        let newData = await axios[type](url,data);
        return newData.data;
    }

}

export default axiosProviders;