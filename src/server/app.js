const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const mongoClient = require("mongodb").MongoClient;
const bcrypt = require('bcryptjs');
const saltRounds = 10;
const allowCrossDomain = (req, res, next) => {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type,Access-Control-Allow-Methods');
    res.header('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
    next();
}
const url = "mongodb://localhost:27017/";

app.use(allowCrossDomain);
app.use(bodyParser.json())

app.use(bodyParser.urlencoded({
    extended: true

}))

mongoClient.connect(url, {useNewUrlParser: true}, (err, client) => {
    const db = client.db("users");
    const collection = db.collection("users");
    app.post('/registr', async (req, res) => {
        const {email} = req.body;
        let msg = '';
        const user = await collection.findOne({email});
        if (!!user) {
            msg = "Пользователь с таким email уже существует!";
        } else {
            msg = "Успешная регистрация";
            req.body.password = await bcrypt.hash(req.body.password, saltRounds);
            collection.insertOne(req.body, () => {
            });
        }
        res.send(msg);
    })
    app.post('/author', async (req, res) => {
        const {username} = req.body;
        const data = await collection.findOne({username});
        const password = await bcrypt.compare(req.body.password, data.password);
        let user = {};
        let msg = '';
        if (!data) {
            msg = 'Такой пользователь не найден!ss';
            user = undefined
        } else if (password) {
            msg = 'Успешная авторизация';
            user = data
        } else {
            msg = 'Введеный пароль не правильный!';
            user = undefined
        }
        res.send({
            data: user,
            msg: msg
        })
    })

})


app.listen(3000, () => {
    console.log('Вы подключились к lolcalhost:3000')
});