import React from 'react'
import axios from 'axios'
import 'babel-polyfill'
import hashPassword from '../../providers/hashPassword'
import axiosProviders from '../../providers/axiosProviders'


class App extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            registr: {
                email: '',
                username: '',
                password: ''
            },
            authorization: {
                username: '',
                password: ''
            },
            user: {},
            sendData: {},
            registrMessage:'Зарегестрируйтесь',
            authMessage: 'Введите логин и пароль'
        }
    }


    async reciveRequest(url,type,nameState){
        let newPassword = await hashPassword.hashingPassword(nameState.password);
        this.setState({
            sendData:{
                ...this.state[nameState],
                password:newPassword

            }
        })
        let newData = await axiosProviders.createRequest(url,type,this.state.sendData);
        return newData;

    }


    registrationChange(event) {
        const {name,value} = event.target;
        this.setState({
            registr: {
                ...this.state.registr,
                [name]: value
            }
        })

    }

    async registrationClick() {
        let newData = await this.reciveRequest('http://localhost:3000/registr', 'post' , 'registr');
        this.setState({
            registrMessage:newData
        })
    }

    authorizationChange(event) {
        const {name,value} = event.target;
        this.setState({
            authorization: {
                ...this.state.authorization,
                [name]:value
            }
        })
    }

    async authorizationClick() {
        let newData = await this.reciveRequest('http://localhost:3000/author', 'post' , 'authorization');
        this.setState({
            user:newData.data,
            authMessage:newData.msg
        })
    }


    render() {
        return (
            <div>
                <button onClick={() => {
                    console.log(this.state);
                }}>
                    Check state
                </button>
                <div>
                    <input name="email" placeholder="Enter your email" type="text"
                           onChange={this.registrationChange.bind(this)}/>
                    <input name="username" placeholder="Enter your username" type="text"
                           onChange={this.registrationChange.bind(this)}/>
                    <input name="password" placeholder="Enter your password" type="text"
                           onChange={this.registrationChange.bind(this)}/>
                    <button onClick={this.registrationClick.bind(this)}>
                        Sign Up
                    </button> {this.state.registrMessage}
                </div>
                <div>
                    <input name="username" placeholder="Enter your username" type="text"
                           onChange={this.authorizationChange.bind(this)}/>
                    <input name="password" placeholder="Enter your password" type="text"
                           onChange={this.authorizationChange.bind(this)}/>
                    <button onClick={this.authorizationClick.bind(this)}>
                        Sign In
                    </button> {this.state.authMessage}
                </div>
            </div>
        )
    }
}

export default App