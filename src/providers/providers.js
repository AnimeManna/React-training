import axios from 'axios'
import 'babel-polyfill';

class Providers{

    constructor() {
    }


    static async createRequest(url,type){
        let newData = await axios[type](url)
        if(newData.data.success==true){
            console.log(newData.data.msg);
        }else{
            console.log(newData.data.msg);
        }
        return newData.data.data;
    }

    static async createResponse(url,type,data) {
        let newData = await axios[type](url,data)
        return newData;
    }
}

export default Providers